import requests
import json
import os

from twilio.rest import Client

def main():
    scrapinghub_url = 'https://careers-page.workable.com/api/v3/accounts/scrapinghub/jobs'
    payload = {
        "query": "",
        "location": [],
        "department": [],
        "worktype": [],
        "remote": []
    }
    response = requests.post(scrapinghub_url, json=payload)

    if response.status_code != 200:
        print(f'invalid response code: {response.status_code} ({response.url})')
        return

    jobs = filter_jobs(response)
    trigger_alert(jobs) if jobs else print('no related job found.')


def filter_jobs(response):
    desired_keywords = ['python', 'software engineer']
    desired_jobs = []

    for job in json.loads(response.text)['results']:
        if any([k in job['title'].lower() for k in desired_keywords]):
            desired_jobs.append(f"Scrapinghub: {job['title']}")

    return desired_jobs


def trigger_alert(jobs):
    message_text = 'Hy Arslan: We have found some jobs you may interested in\n'
    message_text += '\n'.join(jobs)
    print(message_text)

    if not is_env_valid():
        print('To get alert message, please setup environment first.')
        return

    account_sid = os.environ['TWILIO_ACT_SID']
    auth_token = os.environ['TWILIO_AUTH_TOKEN']
    client = Client(account_sid, auth_token)

    client.messages \
        .create(
        body=message_text,
        from_=os.environ['TWILIO_NUMBER'],
        to=os.environ['MY_NUMBER']
    )

    print('alert generated successfully.')


def is_env_valid():
    env_vars = ['TWILIO_ACT_SID', 'TWILIO_AUTH_TOKEN', 'TWILIO_NUMBER', 'MY_NUMBER']
    return all([os.environ.get(env_var) for env_var in env_vars])


if __name__ == "__main__":
    main()
